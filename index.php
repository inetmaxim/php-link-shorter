<?php
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_startup_errors', 0);
ini_set('display_errors', 0);

define('_CHAR_LIST','abcdefghijklmnopqrstuvwxyz0123456789_-ABCDEFGHIJKLMNOPQRSTUVWXYZ');

$out=$func="";
$path = substr($_SERVER['REQUEST_URI'],1);

if (!empty($_REQUEST['cmd'])!='') {
  $func = "ajax_".trim($_REQUEST['cmd']);
}
else {

  $p = strpos($path, "?");
  if ($p !== false) {
  	$path = substr($path, 0, $p);
    }
 
  if (!file_exists('config.php')) {
    $func = "page_setup";
  }
  else if (empty($path)) {
    $func = "page_index";
  } 
  else {
    $func = "page_redirect";
  }  
}

if ($func!='' && function_exists($func))
  $out = $func($path);

if (is_array($out)) {
  header('Content-Type: application/json');
  print json_encode($out, JSON_PRETTY_PRINT);
  }
else print $out;
exit;


function page_index($info='') {
  $out = "
  <html>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js' referrerpolicy='no-referrer'></script>
  <style>
  body {
    height: 100%;
    margin: 0;
  }
  .main {
    height: -webkit-fill-available;
    display: flex;
    align-content: center;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }
  .main p { 
    color: red;
  }
  .dn {
    display: none;
  }
  #out pre a {
    font-size: large;
    color: blue; 
  }
  </style>
  <body>
  <div class='main'>
    <p>$info</p>
    <h1>System of link shortening</h1>
    <form id='frm'>
      <div>
        <label>Enter URL</label>
        <input type='text' id='url'>
        <button>Get short link</button>
      </div>
    </form>
    <div class='container dn' id='out'>
      <pre></pre>
    </div>
    <div class='container dn' id='new'><button>Try again</button></div>
  </div>
  </body>
  <script>
  $('#frm').submit(function(event) {
      event.preventDefault();
  });
  $('#new button').on('click',function() {
    $('#url').val('');
    $('#frm').show();
    $('.container').hide();
  });
  $('#frm button').on('click',function(){
    $.ajax({
    url: '?ts='+Date.now(),
    type: 'POST',
    dataType: 'json',
    data: {cmd:'generate',path:$('#url').val().trim()}
    })
    .done(function (data) {
        console.log(data);
        if (data.status=true) {
          // show and open
          $('#frm').hide();
          $('#out pre').html(data.link);
          $('.container').show();
          }
        else {
          // close and hide
          $('#out pre').html('Error');
          }
        })
    .fail(function (request,error) {
            alert('Network error has occurred please try again!');
        });
  });
  </script>
  </html>
  ";
  return $out;
}

function page_setup() {
  $connect=[
    'server'=>'localhost',
    'user'=>'',
    'pass'=>'',
    'db'=>'',
    'port'=>'3306',
  ];  
  $table_body = '';
  foreach($connect as $k=>$v) {
    $table_body.="<tr><td><label>$k</lable><td><td><input name='connect[$k]' value='$v'></td></tr>";
  }


  $out = "
  <html>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js' referrerpolicy='no-referrer'></script>
  <style>
  body {
    height: 100%;
    margin: 0;
  }
  .main {
    display: flex;
    align-content: center;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }
  #info {
    color:red;
  }
  .dn {
    display: none;
  }
  </style>
  <body>
  <div class='main'>
    <h1>Configuration MySQL</h1>
    <form id='frm'><input name='cmd' value='' id='cmd' type='hidden'>
      <table>
      $table_body
      </table>
    </form>
    <div id='info'></div>
    <div>
      <button class='tool' id='check'>Check connection</button>
      <button class='tool dn' id='save'>Save configuration</button>
    </div>
  </div>
  </body>
  <script>
  $('#frm').submit(function(event) {
      event.preventDefault();
  });

  $('#save').on('click',function() {
    $('#cmd').val('save');
    $.ajax({
    url: '?ts='+Date.now(),
    type: 'POST',
    dataType: 'json',
    data: $('#frm').serialize()
    })
    .done(function (data) {
        if (data.status==true) {
          window.location.href='/';
          }
        else {
          $('.main').html(data.html);
          }
        })
    .fail(function (request,error) {
            alert('Network error has occurred please try again!');
        });
  });
  $('#check').on('click',function(){
    $('#info').text('');
    $('#cmd').val('check');
    $.ajax({
    url: '?ts='+Date.now(),
    type: 'POST',
    dataType: 'json',
    data: $('#frm').serialize()
    })
    .done(function (data) {
        if (data.status==true) {
          // show and open
          $('#save').show();
          $('#check').hide();
          $('#info').text('Connection success');
          }
        else {
          // close and hide
          $('#info').text('Connection error');
          }
        })
    .fail(function (request,error) {
            alert('Network error has occurred please try again!');
        });
  });
  </script>
  </html>
  ";
  return $out;
}

function page_redirect($path) {
  $out='';
  $p = strpos($path, "/");
  if ($p !== false) {
  	$path = substr($path, 0, $p);
    }

  new DB;
  $id = convBase($path,_CHAR_LIST,'0123456789abcdef');
  $row = DB::fetch("SELECT url FROM short_links WHERE id='$id'");
  if (!empty($row)) {
    header("Location: {$row['url']}", true, 303); 
    exit;
  } 
  else {
    header("HTTP/1.1 404 Not Found");
    header("Status: 404 Not Found");
    $out = page_index("Error: link not found");
  }
  return $out;
}

function ajax_check() {
  $out = [];
  $DB = new DB($_REQUEST['connect']);
  if ($DB->link->connect_errno) {
    $out['status']=false;
  }
  else {
    $out['status']=true;
  }
  return $out;
}

function ajax_save() {
  $out = [];
  $c=$_REQUEST['connect'];
  $DB = new DB($c);
  DB::query("CREATE TABLE `short_link` ( `id` VARCHAR(13) NOT NULL , `url` VARCHAR(255) NOT NULL , UNIQUE `id` (`id`)) ENGINE = InnoDB;");

  $file = "<"."?php
\$connect=[
  'server'=>'{$c['server']}',
  'user'=>'{$c['user']}',
  'pass'=>'{$c['pass']}',
  'db'=>'{$c['db']}',
  'port'=>'{$c['port']}',
];
  ";  

  $r = file_put_contents('config.php',$file);
  if ($r===false) {
    $out['status']=false;
    $out['html']="Create file config.php<br>
    <pre>".htmlspecialchars($file)."</pre>";
  }
  else {
    $out['status']=true;
  }
  return $out;
}


function ajax_generate() {
  new DB;
  $url = $_REQUEST['path'];
  
  $out=[];
  
  $row = DB::fetch("SELECT id FROM short_links WHERE url='".DB::escape($url)."'");
  if (!empty($row)) {
    $out['path'] = convBase($row['id'],'0123456789abcdef',_CHAR_LIST);
  }
  else {
    $id = uniqid();
    $out['path'] = convBase($id,'0123456789abcdef',_CHAR_LIST);
    DB::query("INSERT INTO short_links VALUES('$id','".DB::escape($url)."')");
  }  
  if (!empty($out)) {
    $out['link'] = "<a href='//".$_SERVER['SERVER_NAME']."/{$out['path']}' target='_new'>{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['SERVER_NAME']}/{$out['path']}</a>"; 
    $out['status']=true;
  }  

  return $out;  
}


function convBase($numberInput, $fromBaseInput, $toBaseInput)
{
    if ($fromBaseInput==$toBaseInput) return $numberInput;
    $fromBase = str_split($fromBaseInput,1);
    $toBase = str_split($toBaseInput,1);
    $number = str_split($numberInput,1);
    $fromLen=strlen($fromBaseInput);
    $toLen=strlen($toBaseInput);
    $numberLen=strlen($numberInput);
    $retval='';
    if ($toBaseInput == '0123456789')
    {
        $retval=0;
        for ($i = 1;$i <= $numberLen; $i++)
            $retval = bcadd($retval, bcmul(array_search($number[$i-1], $fromBase),bcpow($fromLen,$numberLen-$i)));
        return $retval;
    }
    if ($fromBaseInput != '0123456789')
        $base10=convBase($numberInput, $fromBaseInput, '0123456789');
    else
        $base10 = $numberInput;
    if ($base10<strlen($toBaseInput))
        return $toBase[$base10];
    while($base10 != '0')
    {
        $retval = $toBase[bcmod($base10,$toLen)].$retval;
        $base10 = bcdiv($base10,$toLen,0);
    }
    return $retval;
}

/*


*/


class DB {
	var $link=null;
	var $query=null;

  function __construct() {
    if (isset($GLOBALS['DB']->link)) {return;}
    $connect=[];
    if (func_num_args()==1) { 
      $connect=func_get_arg(0);
    }
    else if (file_exists('config.php')) {
      include_once('config.php');
    }
    else {
      die("Config file not found");
    }
    
    if (empty($connect)) {
      die("Error config file");
    }

    $this->link = new mysqli($connect['server'], $connect['user'], $connect['pass'], $connect['db'],$connect['port']);
    if ($this->link->connect_errno) {
      return;
    }
    $GLOBALS['DB']= $this;
    return($this);
    }

  static function escape($string) {
    if (is_string($string)) return($GLOBALS['DB']->link->real_escape_string($string));
    else return $string;
  }

	static function query($q="") {
		if ($q=="") { return; }

    $GLOBALS['DB']->query=$q;
		if (!$result=$GLOBALS['DB']->link->query($q)) {
      $error = $GLOBALS['DB']->link->error;
      die($error);
      }

		return $result;
		}

  /**
   * Fetch one line of query
   * @param string $q Mysql query
   */
	static function fetch($q="") {
		if ($q=="") {return [];}
    $res = self::query($q);
    if (is_object($res)) return self::get($res);
    else {
      self::errorlog("Not resourse"); return false;}
	  }
	static function get($res) {
    if (is_object($res)) {
		  return(mysqli_fetch_assoc($res));
    }
    else return [];
	}

}