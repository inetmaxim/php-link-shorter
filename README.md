# Php Link Shorter

Example project for system of link shortening

<p align="center"><img src="https://www.php.net/images/logos/new-php-logo.png"></p>

## Task project

You need to create a link shortener:

1. The site visitor enters any original URL in the input field, like http://domain/any/path/etc.;
2. Presses the submit button;
3. The page makes an ajax request to the server and receives a unique short URL;
4. The short url is displayed on the page as http://yourdomain/abCdE (don't use any external services like goo.gl, etc.);
5. The visitor can copy the short URL and repeat the process with a different link

The short URL must be unique, redirect to the original link, and be relevant forever, no matter how many times it is used.


## Requirements

1. php 
2. apache (with mod_rewrite)
3. MySQL or MariaDB

## Installation

Создать виртуальный хост и поместить в корень содержимое репозитория

Запустить в браузере корневой url виртуального сайта.

Следуя инструкции сконфигурировать соединение с MySQL


